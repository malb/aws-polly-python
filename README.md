# AWS Polly in Python #

Render texts to MP3 using different voices for quote blocks (but not inline quotes).

## Requirements ##

1. an [AWS account](https://aws.amazon.com/)
2. [pandoc](http://pandoc.org) for converting to plain text
3. [ffmpeg](https://www.ffmpeg.org) for concatenating mp3s

## Getting Started ##

Create a Python3 [virtualenv](https://virtualenvwrapper.readthedocs.io/en/latest/) for your project

    $ mkvirtualenv --python=/usr/bin/python3 aws-polly

Install requirements

    $ (aws-polly) pip install -r requirements
    
[Configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) the AWS command line interface

    $ (aws-polly) aws configure

Test it

    $ (aws-polly) python ./polly.py README.md README.mp3
    
