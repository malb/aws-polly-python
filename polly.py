#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Render text to mp3 using AWS Polly.
"""

import re
import begin
import requests
import subprocess
import tempfile
import os
import shutil
import datetime
from mutagen.mp3 import MP3

working_directory = tempfile.mkdtemp()
print("working in {}".format(working_directory))


def temp_file(name):
    """Return full path for temporary file ``name``

    :param name: a valid file name

    """
    return os.path.join(working_directory, name)


def plaintextify(output_filename, input_filename):
    """
    Run ``pandoc`` to convert to plain text.

    :param output_filename: name of plain text file
    :param input_filename: name of any pandoc supported input file

    """
    return subprocess.call(
        ["pandoc", input_filename, "--wrap", "None", "-t", "plain", "-o", output_filename]
    )


def load_file(basename, url):
    """Load file from ``url`` and store in ``basename``

    The file extension of ``basename`` is inherited from ``url``.

    :param basename: output base name
    :param url: input URL/basename

    """

    filename = basename + "." + url[url.rfind(".") + 1 :]

    if url.startswith("http"):
        open(filename, "w").write(requests.get(url).text)
    else:
        shutil.copyfile(url, filename)
    return filename


def aws_polly(text, output_filename, voice="Amy"):
    """Call AWS Polly on ``text`` writing to output mp3 file.

    :param text: text to speak
    :param output_filename: name of mp3 file
    :param voice: voice to use

    """

    command = [
        "aws",
        "polly",
        "synthesize-speech",
        "--output-format",
        "mp3",
        "--voice-id",
        voice,
        "--text",
        '"%s"' % text,
        output_filename,
    ]
    if subprocess.call(command) != 0:
        raise RuntimeError("aws polly failed with input {}".format(" ".join(command)))


def concat_mp3s(output_filename, files):
    """
    Concatenate list of mp3 files to one file.

    :param output_filename: file name of output mp3
    :param files: list names of of mp3 files

    """
    with open(temp_file("files.txt"), "w") as fh:
        for file in files:
            fh.write("file '%s'\n" % file)

    return subprocess.call(
        [
            "ffmpeg",
            "-y",
            "-f",
            "concat",
            "-safe",
            "0",
            "-i",
            temp_file("files.txt"),
            "-acodec",
            "mp3",
            output_filename,
        ]
    )


def make_silence(output_filename, length=1.0):
    """
    Write mp3 file with ``length`` seconds of silence to ``output_filename``

    :param output_filename: mp3 filename, file will be overwritten
    :param length: number of seconds, fractions are allows

    """
    return subprocess.call(
        [
            "ffmpeg",
            "-f",
            "lavfi",
            "-i",
            "anullsrc=r=22050:cl=mono",
            "-t",
            str(length),
            "-q:a",
            "9",
            "-acodec",
            "libmp3lame",
            output_filename,
        ]
    )


def is_quote(line):
    """
    Return ``True`` if ``line`` is a quote.
    """
    # pandoc puts "  " before quote blocks in plain text outout
    return line.startswith("  ")


def is_empty_line(line):
    """
    Return ``True`` if ``line`` is empty.
    """
    return line == "\n"


def remove_markup(line):
    """
    Remove all markup from line which might sound funny when pronounced.
    """
    line = line.replace("_", "")  # TODO: this a bit of a hack
    line = re.sub(r"\[\^([0-9]+)\]", "", line)
    return line


def render_srt(output_filename, segments):
    with open(output_filename, "w") as fh:
        for i, start, stop, text in segments:
            fh.write("{i}\n".format(i=i))
            start = datetime.datetime(1970, 1, 1) + start
            stop = datetime.datetime(1970, 1, 1) + stop
            startm = start.microsecond // 1000
            stopm = stop.microsecond // 1000
            fh.write(
                ("{start},{startm:03d}" " --> " "{stop},{stopm:03d}\n").format(
                    start=start.strftime("%H:%M:%S"),
                    stop=stop.strftime("%H:%M:%S"),
                    startm=startm,
                    stopm=stopm,
                )
            )
            fh.write("{s}\n".format(s=text))


@begin.start(auto_convert=True)
def main(
    filename: "Filename to speak",
    output_filename: "Output filename",
    main_voice: "Voice for reading main text" = "Emma",
    quote_voice: "Voice for reading block quotes" = "Brian",
    paragraph_break: "Length of silence between paragraphs in seconds" = 1.0,
    limit: "Only process this many paragraphs" = 0,
    srt: "Also write an srt file" = False,
):
    """
    Render text document to mp3 using AWS Polly.

    Call ``aws polly describe-voices`` to see available voices.

    """

    plaintextify(temp_file("text.txt"), load_file(temp_file("text"), filename))

    make_silence(temp_file("silence.mp3"), length=paragraph_break)

    # process first, catching any potential errors before making costly calls to aws polly.
    lines = []
    i = 0
    for line in open(temp_file("text.txt")).readlines():
        if is_empty_line(line):
            if lines and is_empty_line(lines[-1]):
                continue
        else:
            i += 1

        line = remove_markup(line)

        if len(line) > 1500:
            fmt = "AWS Polly will only accept up to 1500 characters per invocation, but line '%s…' has %d characters"
            raise ValueError(fmt % (line[:20], len(line)))

        lines.append(line)

        if limit and i >= limit:
            break

    mp3_files = []
    srt_segments = []
    timestamp = datetime.timedelta(seconds=0.5)
    length = 0.0
    counter = 1

    for i, line in enumerate(lines):

        if is_empty_line(line):
            fn = temp_file("silence.mp3")
            mp3_files.append(fn)
            timestamp += datetime.timedelta(seconds=paragraph_break)
            continue

        fn = temp_file("text%06d.mp3" % (i,))
        if is_quote(line):
            aws_polly(line, fn, voice=quote_voice)
        else:
            aws_polly(line, fn, voice=main_voice)

        mp3_files.append(fn)
        length = datetime.timedelta(seconds=MP3(fn).info.length)
        srt_segments.append((counter, timestamp, timestamp + length, line))
        timestamp += length
        counter += 1

    if srt:
        render_srt(output_filename.replace(".mp3", ".srt"), srt_segments)
    concat_mp3s(output_filename, mp3_files)
